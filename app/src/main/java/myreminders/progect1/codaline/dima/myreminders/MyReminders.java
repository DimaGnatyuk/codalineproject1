package myreminders.progect1.codaline.dima.myreminders;

import android.app.Application;

import myreminders.progect1.codaline.dima.myreminders.connector.HelperFactory;

/**
 * Created by Admin on 07.03.2016.
 */
public class MyReminders extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        HelperFactory.setHelper(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        HelperFactory.releaseHelper();
    }
}
