package myreminders.progect1.codaline.dima.myreminders.entity;

import android.provider.BaseColumns;

/**
 * Created by Admin on 05.03.2016.
 */
public abstract class NoteEntry implements BaseColumns {

    public static final String TABLE = "note";
    public static final String COLUMN_NAME_ID = "id";
    public static final String COLUMN_NAME_TITLE = "title";
    public static final String COLUMN_NAME_DESCRIPTION = "description";
    public static final String COLUMN_NAME_ACTING = "acting";

    /*public static final String SQL_CREATE_TABLE = "CREATE TABLE "+TABLE+" ("+
            COLUMN_NAME_ID + FactoryType.PRIMARY_KEY+","+
            COLUMN_NAME_DESCRIPTION + FactoryType.STRING+","+
            COLUMN_NAME_TITLE + FactoryType.STRING+","+
            COLUMN_NAME_ACTING + FactoryType.STRING+");";*/

    public static final String SQL_DELETE_TABLE = "DROP TABLE IF EXISTS "+TABLE;

}
