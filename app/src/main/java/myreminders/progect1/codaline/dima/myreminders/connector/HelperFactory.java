package myreminders.progect1.codaline.dima.myreminders.connector;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;

import myreminders.progect1.codaline.dima.myreminders.connector.component.DatabaseHelper;

/**
 * Created by Admin on 07.03.2016.
 */
public class HelperFactory {
    private static DatabaseHelper databaseHelper;

    public static DatabaseHelper getHelper(){
        return databaseHelper;
    }
    public static void setHelper(Context context){
        databaseHelper = OpenHelperManager.getHelper(context, DatabaseHelper.class);
    }
    public static void releaseHelper(){
        OpenHelperManager.releaseHelper();
        databaseHelper = null;
    }
}
