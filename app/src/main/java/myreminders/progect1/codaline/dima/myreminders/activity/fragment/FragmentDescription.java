package myreminders.progect1.codaline.dima.myreminders.activity.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.sql.SQLException;

import myreminders.progect1.codaline.dima.myreminders.R;
import myreminders.progect1.codaline.dima.myreminders.connector.HelperFactory;
import myreminders.progect1.codaline.dima.myreminders.entity.Note;

/**
 * Created by Admin on 07.03.2016.
 */
public class FragmentDescription extends Fragment {

    private EditText title;
    private EditText description;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_description, null);
        Button saveKey = (Button) view.findViewById(R.id.onSave);
        title = (EditText) view.findViewById(R.id.textTitle);
        description = (EditText) view.findViewById(R.id.textDescription);
        saveKey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (title.getText().length() == 0)
                    {
                        Toast.makeText(v.getContext(), getString(R.string.message_null), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    Note note = new Note(title.getText().toString(),description.getText().toString());
                    HelperFactory.getHelper().getDaoNote().create(note);
                    Toast.makeText(v.getContext(), getString(R.string.add_to_db), Toast.LENGTH_SHORT).show();
                    title.setText("");
                    description.setText("");
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });
        Button closedKey = (Button) view.findViewById(R.id.onClose);
        closedKey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(), "KeyClose", Toast.LENGTH_SHORT).show();

            }
        });
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }
}
