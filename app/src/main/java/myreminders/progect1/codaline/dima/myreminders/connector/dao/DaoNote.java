package myreminders.progect1.codaline.dima.myreminders.connector.dao;

import android.database.SQLException;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.util.List;

import myreminders.progect1.codaline.dima.myreminders.entity.Note;

/**
 * Created by Admin on 07.03.2016.
 */
public class DaoNote extends BaseDaoImpl<Note, Integer> {

    public DaoNote(ConnectionSource connectionSource,
                   Class<Note> dataClass) throws SQLException, java.sql.SQLException {
        super(connectionSource, dataClass);
    }

    public List<Note> getAllNote() throws SQLException, java.sql.SQLException {
        return this.queryForAll();
    }

    public Note getNoteById(int id) throws SQLException, java.sql.SQLException {
        return this.queryForId(id);
    }

    public void deleteNote(Note note) throws SQLException, java.sql.SQLException {
        this.delete(note);
    }

    public void updateNote (Note note) throws SQLException, java.sql.SQLException {
        this.update(note);
    }

    public void clearDataBase () throws java.sql.SQLException {
        this.delete(this.getAllNote());
    }
}
