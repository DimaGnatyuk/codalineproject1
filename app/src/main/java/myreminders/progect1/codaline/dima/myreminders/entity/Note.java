package myreminders.progect1.codaline.dima.myreminders.entity;

/**
 * Created by Admin on 05.03.2016.
 */

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
@DatabaseTable(tableName="Note")
public class Note extends NoteEntry{
    @DatabaseField(generatedId = true, dataType=DataType.INTEGER, canBeNull = false, columnName = "id")
    private int id;
    @DatabaseField(dataType=DataType.STRING)
    private String title;
    @DatabaseField(dataType=DataType.STRING)
    private String description;
    @DatabaseField(dataType=DataType.BOOLEAN)
    private boolean acting;

    public Note (){

    }
    public Note(boolean acting, String title, String description, int id) {
        this.acting = acting;
        this.title = title;
        this.description = description;
        this.id = id;
    }

    public Note(String title, String description) {
        this.description = description;
        this.title = title;
    }

    public boolean isActing() {
        return acting;
    }

    public void setActing(boolean acting) {
        this.acting = acting;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
