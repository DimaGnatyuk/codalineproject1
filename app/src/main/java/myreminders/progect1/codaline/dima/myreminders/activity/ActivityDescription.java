package myreminders.progect1.codaline.dima.myreminders.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.sql.SQLException;

import myreminders.progect1.codaline.dima.myreminders.R;
import myreminders.progect1.codaline.dima.myreminders.connector.HelperFactory;
import myreminders.progect1.codaline.dima.myreminders.entity.Note;

public class ActivityDescription extends AppCompatActivity {
    public static final String KEY_ID_CLASS = "idKey";
    private EditText title;
    private EditText description;
    private CheckBox checkBox;
    private Note note = null;
    /*
    ***
    Appointment: Playing and editing Note
    ORM: Lite ORM (package connect...), use HelperFactory
    ***
    */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_description);
        title = (EditText) findViewById(R.id.activity_description_textTitle);
        description = (EditText) findViewById(R.id.activity_description_textDescription);
        checkBox = (CheckBox) findViewById(R.id.activity_description_checkBox);
        Button save = (Button)findViewById(R.id.activity_description_onSave);
        Button close = (Button)findViewById(R.id.activity_description_onClose);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Button delete = (Button)findViewById(R.id.activity_description_onDelete);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (title.getText().length() == 0)
                {
                    Toast.makeText(v.getContext(), getString(R.string.message_null), Toast.LENGTH_SHORT).show();
                    return;
                }
                note.setTitle(title.getText().toString());
                note.setDescription(description.getText().toString());
                note.setActing(checkBox.isChecked());
                try {
                    HelperFactory.getHelper().getDaoNote().updateNote(note);
                    finish();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    HelperFactory.getHelper().getDaoNote().deleteNote(note);
                    finish();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });
        try {
            note = HelperFactory.getHelper().getDaoNote().getNoteById(getIntent().getExtras().getInt(KEY_ID_CLASS));
            /*
            * Loaded object Note by Id KEY_ID_CLASS
            * id -> Intent saved by Key
            * */
        } catch (SQLException e) {
            e.printStackTrace();
        }
        title.setText(note.getTitle());
        description.setText(note.getDescription());
        checkBox.setChecked(note.isActing());
    }
}
