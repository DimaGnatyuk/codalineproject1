package myreminders.progect1.codaline.dima.myreminders.activity.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.sql.SQLException;

import myreminders.progect1.codaline.dima.myreminders.R;
import myreminders.progect1.codaline.dima.myreminders.connector.HelperFactory;

/**
 * Created by Admin on 08.03.2016.
 */
public class FragmentHelloLogo extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = null;
        try {
            if (HelperFactory.getHelper().getDaoNote().getAllNote().size() == 0)
            {
                view = inflater.inflate(R.layout.fragment_hello,null);
            }else{
                view = super.onCreateView(inflater, container, savedInstanceState);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return view;
    }
}
