package myreminders.progect1.codaline.dima.myreminders.activity.fragment.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import myreminders.progect1.codaline.dima.myreminders.R;
import myreminders.progect1.codaline.dima.myreminders.entity.Note;

public class CustomAdapter extends ArrayAdapter {
    public CustomAdapter(Context context, int resource) {
        super(context, resource);
    }

    public CustomAdapter(Context context, int resource, List<Note> objects) {
        super(context, resource, objects);
    }

    public CustomAdapter(Context context, int resource, int textViewResourceId, List<Note> objects) {
        super(context, resource, textViewResourceId, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_view_all, null);
        }
        TextView title = (TextView) convertView.findViewById(R.id.title);
        TextView description = (TextView) convertView.findViewById(R.id.description);
        ImageView imageView = (ImageView) convertView.findViewById(R.id.image);

        Note note = (Note)getItem(position);
        String titleString = note.getTitle().toString();
        String descriptionString = note.getDescription().toString();
        if (titleString.length()<20)
        {
            title.setText(titleString);
        }else{

            title.setText(titleString.substring(0,20)+"...");
        }

        if (descriptionString.length()<25)
        {
            description.setText(descriptionString);
        }else{
            description.setText(descriptionString.substring(0,25)+"...");
        }


        if (note.isActing()) {
            imageView.setImageResource(R.drawable.progress_ok);
        }else{
            imageView.setImageResource(R.drawable.process);
        }
        return convertView;
    }
}
