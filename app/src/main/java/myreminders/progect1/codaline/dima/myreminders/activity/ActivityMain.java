package myreminders.progect1.codaline.dima.myreminders.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.sql.SQLException;

import myreminders.progect1.codaline.dima.myreminders.R;
import myreminders.progect1.codaline.dima.myreminders.activity.fragment.FragmentDescription;
import myreminders.progect1.codaline.dima.myreminders.activity.fragment.FragmentHelloLogo;
import myreminders.progect1.codaline.dima.myreminders.activity.fragment.FragmentViewAll;
import myreminders.progect1.codaline.dima.myreminders.connector.HelperFactory;
import myreminders.progect1.codaline.dima.myreminders.connector.component.DatabaseHelper;
import myreminders.progect1.codaline.dima.myreminders.connector.dao.DaoNote;

public class ActivityMain extends AppCompatActivity {
    private static Toast toast = null;
    private FragmentManager mFragmentManager;
    private AlertDialog.Builder alDialog;
    private FragmentViewAll fragmentViewAll;
    /*
    ***
    Appointment: View element List and Edit structure
    ORM: Lite ORM (package connect...), use HelperFactory
    ***
    */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mFragmentManager = getSupportFragmentManager();
        alDialog = new AlertDialog.Builder(this);
        alDialog.setTitle(getString(R.string.warning));
        alDialog.setMessage(getString(R.string.clear_db)+" ?");
        alDialog.setPositiveButton(getString(R.string.on_delete), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    HelperFactory.getHelper().getDaoNote().clearDataBase();
                    ActionToasd(getString(R.string.clear_db));
                    updateList();
                    updateStatys();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });
        alDialog.setNegativeButton(getString(R.string.on_Close), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        updateList();
        updateStatys();
    }

    void updateList (){
        /*
           Appointment: update View List
         */
        fragmentViewAll = new FragmentViewAll();
        mFragmentManager.beginTransaction().replace(R.id.fragment_container, fragmentViewAll).commit();
    }

    void updateStatys (){
        /*
           Appointment: update fragment.
           Condition: if Object to database = null, View FragmentDescription and added Hello Logo fragment
         */
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        try {
            if (HelperFactory.getHelper().getDaoNote().getAllNote().size() == 0) {
                fragmentTransaction.replace(R.id.fragment_container, new FragmentDescription());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        fragmentTransaction.replace(R.id.fragment_hello_FrameLayout, new FragmentHelloLogo());
        fragmentTransaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.manu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        switch (item.getItemId())
        {
            case R.id.new_create : {
                FragmentDescription fragmentDescription = new FragmentDescription();
                fragmentTransaction.replace(R.id.fragment_container, fragmentDescription);
                break;
            }

            case R.id.open_list : {
                FragmentViewAll fragmentViewAll = new FragmentViewAll();
                fragmentTransaction.replace(R.id.fragment_container, fragmentViewAll);
                break;
            }

            case R.id.clear_db : {
                alDialog.show();
                break;
            }

            case R.id.about : {
                ActionToasd(getString(R.string.text_Author)+": "+getString(R.string.autor_person));
                break;
            }
        }
        fragmentTransaction.commit();
        updateStatys();
        return true;
    }

    private void ActionToasd (String string){
        if (toast == null)
        {
            toast = new Toast(getApplicationContext());
        }else{
            toast.cancel();
        }
        toast.makeText(getApplicationContext(),string,Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        updateStatys();
    }
}
