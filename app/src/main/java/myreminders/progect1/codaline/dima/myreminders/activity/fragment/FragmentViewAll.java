package myreminders.progect1.codaline.dima.myreminders.activity.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import junit.framework.Test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import myreminders.progect1.codaline.dima.myreminders.R;
import myreminders.progect1.codaline.dima.myreminders.activity.ActivityDescription;
import myreminders.progect1.codaline.dima.myreminders.activity.fragment.adapter.CustomAdapter;
import myreminders.progect1.codaline.dima.myreminders.connector.HelperFactory;
import myreminders.progect1.codaline.dima.myreminders.entity.Note;

/**
 * Created by Admin on 06.03.2016.
 */
public class FragmentViewAll extends Fragment {
    private List<Note> notes = null;

    @Override
    public void onStart() {
        super.onStart();
        try {
            notes = HelperFactory.getHelper().getDaoNote().getAllNote();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        CustomAdapter customAdapter = new CustomAdapter(this.getContext(), R.layout.list_item_view_all, notes);
        ListView listView = (ListView) this.getView().findViewById(R.id.listView);
        listView.setAdapter(customAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(),ActivityDescription.class);
                intent.putExtra(ActivityDescription.KEY_ID_CLASS,notes.get(position).getId());
                startActivity(intent);
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_view_all,null,false);
        return view;
    }
}
